#include <iostream>
using namespace std;

class String {
private:
    int size;
    char *string;
public:
    // конструктор по умолчанию
    String() {
        size = 12;
        string = new char[size];
    };
    // перегрузка конструктора
    // взятие строки
    String(char *str) {
        size = strlen(str);
        string = new char[size];
        strcpy(string, str);
    }
    // копирование
    String(String & value) {
        size = value.getSize();
        string = new char[size];
        strcpy(string, value.getString());
    }
    // диструктор по умолчанию
    ~String() {
        delete[] string;
    }
    void output();
    int getSize();
    char* getString();
    void input();
    float average();
    void concatenation(String first, String second);
    int replace(char *find, char *value);
};

int main() {
    int menu;
    String first;
    String second;
    String third;
    // ввод строки
    cout << "Input string: ";
    first.input();
    // вывод средеарифметического чисел в строке
    cout << "Average number: " << first.average() << endl;
    // обьединение строк
    cout << "Input second string: ";
    second.input();
    third.concatenation(first, second);
    cout << "Result: ";
    third.output();
    // замена слов
    int lenFind;
    int lenValue;
    cout << "Input lenght find: ";
    cin  >> lenFind;
    char find[lenFind];
    cout << "Input find:";
    cin.getline(find, lenFind);
    cout << "Input lenght value: ";
    cin  >> lenValue;
    char value[lenValue];
    cout << "Input value: ";
    cin.getline(value, lenValue);
    cout << first.replace(find, value) << " word replace!" << endl;
    cout << "Result: ";
    first.output();
    return 0;
}
// вывод строки
void String::output() {
    cout << string << endl;
}
// получить длинну
int String::getSize() {
    return size;
}
// получить строку
char* String::getString() {
    return string;
}
// ввести строку
void String::input() {
    gets(string);
    size = strlen(string);
}
// среднеарифметическое чисел в строке
float String::average() {
    int count = 0;
    float average = 0;
    for (int i = 0; i < size; i++) {
        if (isdigit(string[i]) && string[i] != '\0') {
            count++;
            average += (int)string[i] - 48;

        }
    }
    count--;
    average--;
    // возвращает ноль если нет чисел
    return count == 0 ? 0 : average / count;
}
// объединение двух строк
void String::concatenation(String first, String second) {
    size = first.getSize() + second.getSize();
    string = new char[size];
    strcat(string, first.getString());
    strcat(string, second.getString());
}
// замена слов
int String::replace(char *find, char *value) {
    int index = 0;
    int count = 0;
    for (int i = 0; i < size; i++) {
        if (string[i] == find[0]) {
            for (int j = 0; j < strlen(find); j++) {
                if (string[i + j] == find[j]) {
                    index++;
                }
            }
            if (index == strlen(find)) {
                count++;
                int range = strlen(value) - strlen(find);
                size += range;
                char *buffer = string;
                string = new char[size];
                for (int j = 0; j < size; j++) {
                    if (j < i) {
                        string[j] = buffer[j];
                    }
                    else if (j > i + range) {
                        string[j] = buffer[j - range];
                    }
                }
                for (int j = 0; j < strlen(value); j++) {
                    string[i + j] = value[j];
                }
            }
        }
    }
    // возвращает количество замен
    return count;
}
