#include "point.cpp"
#include <iostream>
using namespace std;

point::point(double initial_x, double initial_y) {
    x = initial_x;
    y = initial_y;
}

void point::shift(double x_amount, double y_amount) {
    x += x_amount;
    y += y_amount;
}


void point::rotate90() {
    double new_x;
    double new_y;
    new_x = y;
    new_y = -x;
    x = new_x;
    y = new_y;
}

int rotations_needed(point p) {
    int countRotate = 0;
    while (p.get_x() < 0 || p.get_y() < 0) {
        p.rotate90();
        countRotate++;
    }
    return countRotate;
}

void rotate_to_upper_right(point &p) {
    while (p.get_x() < 0 || p.get_y() < 0) {
        p.rotate90();
    }
}

int main() {
    point first(3, -3);
    cout << "Input point"              << endl
         << "X : "    << first.get_x() << endl
         << "Y : "    << first.get_y() << endl   << endl
         << "Need: "  << rotations_needed(first) << endl
         << "Position"<< endl
         << "X : "    << first.get_x() << endl
         << "Y : "    << first.get_y() << endl  << endl;
    rotate_to_upper_right(first);
    cout << "Position"<< endl
         << "X : "    << first.get_x() << endl
         << "Y : "    << first.get_y() << endl;

    return 0;
}
